import math
from Tkinter import *
import Tkinter


'''
This class makes vector type objects.Theses objects have the same parameters with math vectors and in vector class are writen functions which can be done with math vectors
'''



class vector(object):

#This is constructing function of our object which checks coordinates of our vector object and makes vector object
	def __init__(self, x=0, y=0):
		if type(x) == int and type(y) == int:
			self.x = x
			self.y = y
		elif type(x) == float and type(y) == float:
			self.x = x
			self.y = y
		else:
			print 'x, y arguments must be digits'

#__add__ function works with temprary and called object in the same time and summerizes enumbered vector type objects and attributes it to new vector type object.It checks type of added object if new object is vector 
	def __add__(self,other):
		if isinstance(other,vector):
			print "The exiting vector after adding is new = (%d, %d)"%(self.x + other.x,self.y + other.y)

		else:
			print "Trying to combinate nonvector type object with vector type object.Check your argument"


#__iadd__ function adds called object to temprary object aand works like  __add__ function
	def __iadd__(self,other):
		if isinstance(other,vector):
			self.x += other.x
			self.y += other.y
			print "The exiting vector after adding is new = (%d, %d)"%(self.x + other.x,self.y + other.y)
		else:
			print "Trying to combinate nonvector type object with vector type object.Check your argument"

#Like __add__ function
	def __sub__(self,other):
		if isinstance(other,vector):
			print "The exiting vector after subtracting is new = (%d, %d)"%(self.x + other.x,self.y + other.y)
			return vector(self.x - other.x,self.y - other.y)
		else:
			print "Trying to combinate nonvector type object with vector type object.Check your argument"

#Like __iadd__ function
	def __isub__(self,other):
		if isinstance(other,vector):
			self.x -= other.x
			self.y -= other.y
			print "The exiting vector after subtracting is new = (%d, %d)"%(self.x + other.x,self.y + other.y)
		else:
			print "Trying to combinate nonvector type object with vector type object.Check your argument"


#Like __add__ function
	def __mul__(self,other):
		if isinstance(other,vector):
			print "The exiting digit after multiplying is new = (%d)" % (self.x * other.x+self.y * other.y)
			return self.x * other.x+self.y * other.y
		elif type(other) == int or type(other) == float:
			print "The exiting vector after multiplying is new = (%d,%d)" % (self.x * other + self.y * other)			
			return vector(self.x * other, self.y * other)
		else:
			"Vector type object can be multiplicate with digit or vector type object"

#Like __iadd__ function
	def __imul__(self,other):
		if isinstance(other,vector):
			self.x *= other.x
			self.y *= other.y
			print "The exiting digit after multiplying is new = (%d)" % (self.x * other.x+self.y * other.y)
		elif type(other) == int or type(other) == float:
			self.x *= other
			self.y *= other
			print "The exiting vector after multiplying is new = (%d,%d)" % (self.x * other,self.y * other)	
			return vector(self.x, self.y)
		else:
			"Vector type object can be multiplicate with digit or vector type object"


#This function calculates the angle between two vectors and shaws it
	def angle(self,other):
		if isinstance(other,vector):
			alfa = math.acosh(self.x * other.x + self.y * other.y  / (math.sqrt(self.x * self.x + self.y * self.y ) + math.sqrt(other.x * other.x + other.y * other.y)))
			print "The angle between two vectors with radians is equal alfa = %d" % (alfa)
			print "The angle between two vectors with degrees is equal alfa = %d" % (alfa * 3.14)
		else:
			print "There is no any signification to do this operation"
		


#This function uses sqrt function from "math" library and calculates the module of selected vector
	def module(self):
		self.__mod = math.sqrt(self.x * self.x + self.y*self.y)
		print "Length of selected vector is %d"%(self.__mod)



#This function draws on canvas selected vector
        def draw_vector(self):
                dline = canvas.create_line(750, 320, 750+self.x, 320-self.y, arrow = LAST, fill = "navy")

#dadd function draws vector after adding selected vectors each to other
def dadd():
	dline = canvas.create_line(750+v1.x, 320-v1.y, 750+v2.x, 320-v2.y, arrow = LAST, fill = "lime green")

#This function clears canvas when we press command clear
def clear():
	canvas.delete("all")




#This block opens file and select coordinates for vector objects                        
file = open('v1.txt',"r+")
line = file.readlines()
file.close
a = line[0]
b = line[1]
a=int(a)
b=int(b)
file = open('v2.txt',"r+")
line = file.readlines()
file.close
c = line[0]
d = line[1]
c =int(c)
d =int(d)


#This block builds main window
root = Tkinter.Tk()
root.geometry("1440x900")
root.title("Vectors")



#This line makes canvas
canvas = Tkinter.Canvas(root, bg="white smoke", height = 640, width = 1300, cursor = "arrow")

#Making vectors
v1 = vector(a,b)
v2 = vector(c,d)

#"Shaw" menubar building 
menubar = Menu(root)
filemenu = Menu(menubar, tearoff=0)
filemenu.add_command(label="Show 1-st vector", command=v1.draw_vector)
filemenu.add_command(label="Show 2-nd vector", command=v2.draw_vector)
filemenu.add_separator()
filemenu.add_command(label="Show v1+v2", command=dadd )
filemenu.add_separator()
filemenu.add_command(label = "Clear", command=clear)
filemenu.add_separator()
filemenu.add_command(label="Exit", command=root.quit)
menubar.add_cascade(label="Shaw", menu=filemenu)

#Edit menubar building
editmenu = Menu(menubar, tearoff=0)
editmenu.add_command(label="print v1+v2", command=v1.__add__(v2))
editmenu.add_command(label="print v1-v2", command=v1.__sub__(v2))
editmenu.add_command(label="print v2-v1", command=v2.__sub__(v1))
editmenu.add_command(label="print v2+v1", command=v2.__add__(v1))
editmenu.add_command(label="print v1*v2", command=v1.__mul__(v2))
menubar.add_cascade(label="Operations",menu=editmenu)


root.config(menu=menubar)
canvas.pack()
root.mainloop()
